//
//  AppDelegate.swift
//  ResChecker
//
//  Created by Rutkowski, Chris (Agoda) on 9/24/16.
//  Copyright © 2016 Rutkowski, Chris (Agoda). All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {


    func application(_ sender: NSApplication, openFiles filenames: [String]) {
        let dispatchTime: DispatchTime = DispatchTime.now() + Double(Int64(0.1 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: dispatchTime, execute: {
            ViewController.shared.process(files: filenames)
        })

    }

    func applicationDidFinishLaunching(_ aNotification: Notification) {
        print("start")
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }


}

