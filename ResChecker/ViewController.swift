//
//  ViewController.swift
//  ResChecker
//
//  Created by Rutkowski, Chris (Agoda) on 9/24/16.
//  Copyright © 2016 Rutkowski, Chris (Agoda). All rights reserved.
//

import Cocoa

extension NSSize {

    func multiplying(by size: NSSize) {
        return
    }

}

class ResGroup {
    var inch4path: String?
    var inch47path: String?
    var inch55StandardPath: String?
    var inch55ZoomPath: String?

    var standardName: String!

    init?(path: String) {
        guard path.hasSuffix(".png") else {
            return nil
        }

        if path.contains("_1136x640") {
            inch4path = path
        } else if path.contains("_1334x750") {
            inch47path = path
        } else if path.contains("_2208x1242") {
            inch55StandardPath = path
        } else if path.contains("_2001x1125") {
            inch55ZoomPath = path
        } else {
            return nil
        }

        var standardName = path
        standardName = standardName.replacingOccurrences(of: "_1136x640", with: "")
        standardName = standardName.replacingOccurrences(of: "_1334x750", with: "")
        standardName = standardName.replacingOccurrences(of: "_2208x1242", with: "")
        standardName = standardName.replacingOccurrences(of: "_2001x1125", with: "")
        self.standardName = standardName
    }

    var message: String {
        var output = "\(standardName!): "
        if inch4path != nil && inch47path != nil && inch55StandardPath != nil && inch55ZoomPath != nil {
            let inch4 = NSImage(byReferencingFile: inch4path!)
            let inch47 = NSImage(byReferencingFile: inch47path!)
            let inch55Standard = NSImage(byReferencingFile: inch55StandardPath!)
            let inch55Zoom = NSImage(byReferencingFile: inch55ZoomPath!)

            if inch4 == nil {
                output.append("ERROR: 1136x640 corrupted")
            } else if inch47 == nil {
                output.append("ERROR: 1334x750 corrupted")
            } else if inch55Standard == nil {
                output.append("ERROR: 2208x1242 corrupted")
            } else if inch55Zoom == nil {
                output.append("ERROR: 2001x1125 corrupted")
            } else {
                let size4 = inch4!.size
                let size47 = inch47!.size
                let size55Standard = inch55Standard!.size
                let size55Zoom = inch55Zoom!.size


                var expected4Width = Int(round(size47.width * 1136 / 1334))
                if expected4Width % 2 != 0 {
                    expected4Width -= 1
                }
                var expected4Height = Int(round(size47.height * 640 / 750))
                if expected4Height % 2 != 0 {
                    expected4Height -= 1
                }

                var expected55StandardWidth = Int(round(size47.width * 2208 / 1334))
                if expected55StandardWidth % 2 != 0 {
                    expected55StandardWidth += 1
                }
                var expected55StandardHeight = Int(round(size47.height * 1242 / 750))
                if expected55StandardHeight % 2 != 0 {
                    expected55StandardHeight += 1
                }

                let expected55ZoomWidth = Int(round(size47.width * 1.5))
                let expected55ZoomHeight = Int(round(size47.height * 1.5))

                // VALIDATING 4.7
                if Int(size47.width) % 2 != 0 || Int(size47.height) % 2 != 0 {
                    output.append("ERROR: 1334x750 is not valid retina \(Int(size47.width))x\(Int(size47.height)) width and height should be divisable by 2")
                }

                // VALIDATING 4
                else if expected4Width != Int(size4.width) || expected4Height != Int(size4.height) {
                    output.append("ERROR: If 1334x750 is \(Int(size47.width))x\(Int(size47.height)) then 1136x640 should be \(expected4Width)x\(expected4Height) but is \(Int(size4.width))x\(Int(size4.height))")
                }

                // VALIDATING 5.5 Standard
                else if expected55StandardWidth != Int(size55Standard.width) || expected55StandardHeight != Int(size55Standard.height) {
                    output.append("ERROR: If 1334x750 is \(Int(size47.width))x\(Int(size47.height)) then 2208x1242 should be \(expected55StandardWidth)x\(expected55StandardHeight) but is \(Int(size55Standard.width))x\(Int(size55Standard.height))")
                }

                // VALIDATING 5.5 Zoom
                else if expected55ZoomWidth != Int(size55Zoom.width) || expected55ZoomHeight != Int(size55Zoom.height) {
                    output.append("ERROR: If 1334x750 is \(Int(size47.width))x\(Int(size47.height)) then 2001x1125 should be \(expected55ZoomWidth)x\(expected55ZoomHeight) but is \(Int(size55Zoom.width))x\(Int(size55Zoom.height))")
                }
                else {
                    output.append("OK")
                }


            }

        } else {
            output.append("ERROR:")
            if inch4path == nil {
                output.append("missing 1136x640 ")
            }

            if inch47path == nil {
                output.append("missing _1334x750 ")
            }

            if inch55StandardPath == nil {
                output.append("missing 2208x1242 ")
            }

            if inch55ZoomPath == nil {
                output.append("missing 2001x1125 ")
            }
        }

        return output
    }

    func addPath(path: String) -> Bool {
        var standardName = path
        standardName = standardName.replacingOccurrences(of: "_1136x640", with: "")
        standardName = standardName.replacingOccurrences(of: "_1334x750", with: "")
        standardName = standardName.replacingOccurrences(of: "_2208x1242", with: "")
        standardName = standardName.replacingOccurrences(of: "_2001x1125", with: "")

        guard standardName == self.standardName else {
            return false
        }

        if path.contains("_1136x640") {
            inch4path = path

            return true
        } else if path.contains("_1334x750") {
            inch47path = path

            return true
        } else if path.contains("_2208x1242") {
            inch55StandardPath = path

            return true
        } else if path.contains("_2001x1125") {
            inch55ZoomPath = path

            return true
        } else {
            return false
        }
    }

    private func setStandardName(path: String) {
        standardName = (path as NSString).replacingOccurrences(of: "_1136x640", with: "")
    }
}
//
//ekran 4 cal o rozdzielczości 1136x640
//ekran 4.7cal o rozdzielczości 1334x750
//ekran 5.5cal o rozdzielczości 2208x1242 (standard)
//ekran 5.5cal o rozdzielczości 2001x1125 (zoom)

class ViewController: NSViewController {

    @IBOutlet private var textView: NSTextView!

    static var shared: ViewController!

    func process(files: [String]) {
        var output = ""

        var resGroups = [ResGroup]()

        for file in files {
            var added = false
            for resGroup in resGroups {
                if resGroup.addPath(path: file) {
                    added = true
                    break
                }
            }

            if !added {
                if let resGroup = ResGroup(path: file) {
                    resGroups.append(resGroup)
                } else {
                    output.append("\n\n\(file): Unknown file")
                }
            }
        }

        for resGroup in resGroups {
            output.append("\n\n\(resGroup.message)")
        }


        textView.string = output
    }

    override func viewDidLoad() {
        ViewController.shared = self

        super.viewDidLoad()
    }

    override var representedObject: Any? {
        didSet {
        // Update the view, if already loaded.
        }
    }


}

